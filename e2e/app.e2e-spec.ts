import { ThesisAppPage } from './app.po';

describe('thesis-app App', () => {
  let page: ThesisAppPage;

  beforeEach(() => {
    page = new ThesisAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
